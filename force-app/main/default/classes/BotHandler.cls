public interface BotHandler {
    // sample comments for bot response
    BotResponse handle(String utterance, String[] params, Map<String, String> session, String fileName, String fileContent);
    
}